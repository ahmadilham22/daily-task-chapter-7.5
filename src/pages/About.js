import "../App.css";
import Language from "../components/LanguageComponents";
import React, { Component } from "react";

class About extends Component {
  LanguageList = {
    list: [
      {
        id: "HTML & CSS",
        name: "HTML & CSS",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg",
      },
      {
        id: "Javascript",
        name: "Javascript",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg",
      },
      {
        id: "React",
        name: "React",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg",
      },
      {
        id: "Ruby",
        name: "Ruby",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg",
      },
      {
        id: "Ruby on Rails",
        name: "Ruby on Rails",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg",
      },
      {
        id: "Python",
        name: "Python",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg",
      },
      {
        id: "Tes",
        name: "Tes",
        image:
          "https://images.unsplash.com/photo-1534349762230-e0cadf78f5da?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8cGljdHVyZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500",
      },
    ],
  };

  ListLanguage = (language) => {
    return language.map((languageItem) => {
      if (this.props.id === languageItem.name) {
        return (
          <Language
            name={languageItem.name}
            image={languageItem.image}
            isBtn={false}
          />
        );
      } else if (this.props.id === undefined) {
        return (
          <Language
            name={languageItem.name}
            image={languageItem.image}
            isBtn={true}
          />
        );
      }
    });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div class="language-card">
            {this.ListLanguage(this.LanguageList.list)}
          </div>
        </header>
      </div>
    );
  }
}

export default About;

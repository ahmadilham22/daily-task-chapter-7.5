// import "./Detail.css";
// import Language from '../components/LanguageComponent';
import { useParams } from "react-router-dom";
import About from "./About";

function Data() {
  let { id } = useParams();

  return (
    <div className="App">
      <header className="App-header">
        <h1>Bahasa Pemrograman {id}</h1>
        <h1>Id : {id}</h1>
        <About id={id} />
      </header>
    </div>
  );
}
export default Data;
